//VIEILLE VERSION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

//Variables
wchar_t ANormCaract[] = L"1234567890()[]{}=*%$^!:/;,'";
wchar_t Convers[] = L"éèàâêûîôùüëäïö ";
wchar_t GoodConvers[] = L"eeaaeuiouueaio ";
wchar_t AlphabetMinus[] = L"abcdefghijklmnopqrstuvwxyz ";
wchar_t AlphabetMajus[] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

wchar_t message[255];

//Fonctions
int Verification();
void IncorrectVerification();
void ConversionDesAccents();
void Cesar();
void Vigenere();
void Dechiffrage_Cesar();
void Dechiffrage_Vigenere();

void logo(){

printf("==================================================================================================================================================\n");
printf("           _______  _______  __   __  _______    _______  __    _  _______  ______    __   __  _______  _______  ___   _______  __    _ \n");
printf("          |       ||       ||  |_|  ||       |  |       ||  |  | ||       ||    _ |  |  | |  ||       ||       ||   | |       ||  |  | |\n");
printf("          |_     _||    ___||       ||_     _|  |    ___||   |_| ||       ||   | ||  |  |_|  ||    _  ||_     _||   | |   _   ||   |_| |\n");
printf("            |   |  |   |___ |       |  |   |    |   |___ |       ||       ||   |_||_ |       ||   |_| |  |   |  |   | |  | |  ||       |\n");
printf("            |   |  |    ___| |     |   |   |    |    ___||  _    ||      _||    __  ||_     _||    ___|  |   |  |   | |  |_|  ||  _    |\n");
printf("            |   |  |   |___ |   _   |  |   |    |   |___ | | |   ||     |_ |   |  | |  |   |  |   |      |   |  |   | |       || | |   |\n");
printf("            |___|  |_______||__| |__|  |___|    |_______||_|  |__||_______||___|  |_|  |___|  |___|      |___|  |___| |_______||_|  |__|\n");
printf("\n==================================================================================================================================================\n");
printf("----------------------------------------------------------Bienvenue dans TEXT ENCRYPTION----------------------------------------------------------\n");
printf("==================================================================================================================================================\n\n");

}

int main () {
  FILE* log  = fopen("HistoriqueChiffrement.txt", "w");

  char reload = 'Y';
  int decalage;

while (reload == 'Y') {

    system("clear");
    logo();
    
    //ConversionDesAccents();
    printf("\n");
    wchar_t Clee[255];
   	printf("Quel action voulez vous faire ?\n");
    printf("-------------------------------\n\n");
    printf("- Chiffrer un message en code César [1]\n"); 
    printf("- Déchiffrer un message en code César [2]\n");                 
    printf("- Chiffrer un message en code Vigenere [3]\n");
    printf("- Déchiffrer un message en code Vigenere [4]\n");
    printf("- Lecture du Readme [5]\n\n");

   int Choix;
   printf("Votre choix (nombre compris entre 1 et 5) : ");
   scanf("%d", &Choix);

   switch(Choix) {
      case 1 :
         printf("\nQuelle message voulez chiffrer en code César ? ");
         getwchar();
         wscanf(L"%ls", message);
         if (Verification(message) == 0) 
            IncorrectVerification(message);
         
         printf("\nQuel décalage pour le chiffrement voulez vous utiliser (nombre compris entre 0 et 255) ? ");
         scanf("%d", &decalage);
         fprintf(log, "Votre message en language naturel : %ls\n", message);
         Cesar(message, decalage);
         fprintf(log, "Message en code César chiffrer avec un décalage de %d : %ls\n\n", decalage, message);
         break;

      case 2 :

         printf("\nQuelle message voulez déchiffrer en code César ? ");
         getchar();
         scanf("%l[^\n]", message);
         if (Verification(message) == 0) 
            IncorrectVerification(message);

         printf("\nQuelle clée de décalage pour le chiffrement voulez vous utiliser (nombre compris entre 0 et 255) ? ");
         getchar();
         scanf("%d", &decalage);
         fprintf(log, "Votre message en code César : %ls\n", message);
         Dechiffrage_Cesar(message, decalage);
         fprintf(log, "Message en code César déchiffrer avec un décalage de %d : %ls\n\n", decalage, message);
         break;

      case 3 :

         printf("\nQuelle message voulez chiffrer en code Vigenere ? ");
         getchar();
         scanf("%l[^\n]", message);
         Verification(message);
         if (Verification(message) == 0) 
            IncorrectVerification(message);

         printf("Quelle est la clée de chiffrement Vigenere voulez vous utiliser (aucun espace ou caractères spéciaux sont tolérés) ? \n");
         scanf ("%ls", Clee);
         fprintf(log, "Votre message en language naturel : %ls\n", message);
         Vigenere(message, Clee);
         fprintf(log, "Message en code Vigenere chiffrer (clée : %ls) : %ls\n\n", Clee, message);
         break;

      case 4 :

         printf("\nQuelle message voulez déchiffrer en code Vigenere ? ");
         getchar();
         scanf("%l[^\n]", message);
         if (Verification(message) == 0) 
            IncorrectVerification(message);

         printf("Quelle est la clée de chiffrement Vigenere voulez vous utiliser (aucun espace ou caractères spéciaux sont tolérés) ? \n");
         scanf ("%ls", Clee);
         fprintf(log, "Votre message en code Vigenere : %ls\n", message);
         Dechiffrage_Vigenere(message, Clee);
         fprintf(log, "Message en code Vigenere déchiffrer (clée : %ls) : %ls\n\n", Clee, message);
         break;

      case 5 :

         printf("En travaux\n" );
         break;

      default :

         printf("Nombre invalide, merci de recommencer.\n" );
         main();
   }

   printf("\nVoulez vous recommencer un chiffrement ?");
   printf("\n- Pour arrêter le programme [N]"); 
   printf("\n- Pour recommencer [Y]");
   printf("\n\nQuelle est votre choix (Y or N) : ");
   getchar();
   scanf("%c", &reload);

 }
   printf("\n-------------------\n");
   printf("Merci et aurevoir !\n");
   printf("-------------------\n\n");

   fclose(log);
}

int Verification(char texte[255]) {

	for (int i=0; i < strlen(texte); i++) 
   	{
   		for (int i2=0; i2 < sizeof(ANormCaract); i2++)
   		{
   			if (ANormCaract[i2] == texte[i])
   			{
          return 0;
   			}
  	}
   }

   return 1;
}

void IncorrectVerification(char message[255]) {

    int Verif = 0;
    while (Verif == 0) {
      printf("\n");
      printf("Code incorrect !\n");
      printf("Motif : Contient des caractères illégaux (caractères spéciaux, nombre, etc...)\n");
      printf("Veuillez rentrer un nouveau message à coder : ");
      memset(message, '\0', strlen(message) - 1);
      getchar();
      scanf("%[^\n]", message);
      Verif = Verification(message);
    }

}

void ConversionDesAccents(char texte[255]) {

      for (int i=0; i < strlen(texte); i++) 
      {
         for (int i2=0; i2 < sizeof(Convers); i2++) {
            if (texte[i] == Convers[i2] ) {
               texte[i] = GoodConvers[i2];
            }
         }

}
printf("%s", texte);
}

void Cesar(char texte [255], int decalage) {

      int valeur;

      for (int i = 0; i < strlen(texte); i++) {
         int i2 =0;
         while (AlphabetMinus[i2] != texte[i] || AlphabetMinus[i2] == 27) {
            i2 = i2+1;
         }

         if (i2 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (i2+decalage > 26){
               valeur = i2+decalage;
               texte[i] = AlphabetMinus[valeur % 26];
            } else {
               texte[i] = AlphabetMinus[i2+decalage];
         }
      }
      }

      for (int i = 0; i < strlen(texte); i++) {
         int i2 =0;
         while (AlphabetMajus[i2] != texte[i] || AlphabetMajus[i2] == 27) {
            i2 = i2+1;
         }
         if (i2 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (i2+decalage > 26){
               valeur = i2+decalage;
               texte[i] = AlphabetMajus[valeur % 26];
            } else {
               texte[i] = AlphabetMajus[i2+decalage];
         }
      }
      }

      printf("Le cryptage est : %s\n", texte);
 
}

void Dechiffrage_Cesar(char texte [255], int decalage) {

      int valeur;

      for (int i = 0; i < strlen(texte); i++) {
         int i2 =0;
         while (AlphabetMinus[i2] != texte[i] || AlphabetMinus[i2] == 27) {
            i2 = i2+1;
         }
         if (i2 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (i2-decalage < 0){
               valeur = i2-decalage;
               while (valeur < 0) {
                  valeur = valeur+26;
               }
               texte[i] = AlphabetMinus[valeur];
            } else {
               texte[i] = AlphabetMinus[i2-decalage];
         }
      }
      }

      for (int i = 0; i < strlen(texte); i++) {
         int i2 =0;
         while (AlphabetMajus[i2] != texte[i] || AlphabetMajus[i2] == 27) {
            i2 = i2+1;
         }
         if (i2 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (i2-decalage < 0){
               valeur = i2-decalage;
               while (valeur < 0) {
                  valeur = valeur+26;
               }
               texte[i] = AlphabetMajus[valeur];
            } else {
               texte[i] = AlphabetMajus[i2-decalage];
         }
      }
      }

      printf("Déchiffrement du message codé : %s\n", texte);
}


void Vigenere(char texte [255], char Clee [255]) {

    char nouvelleClee[255];

    for(int i = 0, j = 0; i < strlen(texte); i++, j++){
      if (j == strlen(Clee))
          j = 0;

      if (texte[i] == ' ') {
          nouvelleClee[i] = ' ';
          j = j - 1;

      } else { 
        nouvelleClee[i] = Clee[j];
      }
}

    for(int i = 0; i < strlen(texte); i++) {
        int i3;
        int i2;
        int k;
         for (i2 = 0; i2 == 27 || AlphabetMinus[i2] != texte[i]; i2++) {
            k = 0;
         }

         for (i3 = 0; AlphabetMinus[i3] != nouvelleClee[i] || i3 == 27; i3++) {
         }

         if (i2 > 27) {
          for (i2 = 0; AlphabetMajus[i2] != texte[i] || i2 == 27 ; i2++) {
            k = 1;
          }
        }

        if (i3 > 27) { 
         for (i3 = 0; AlphabetMajus[i3] != nouvelleClee[i] || i3 == 27; i3++) {
         }
        }

        if (texte[i] == 'a') {
          k = 0;
        }
        if (texte[i] == 'A') {
          k = 1;
        }

         if (i2 < 27 && i3 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (k == 0) {
               texte[i] = AlphabetMinus[(i3 + i2) % 26];
            } else {
              texte[i] = AlphabetMajus[(i3 + i2) % 26];
            }
         }
      }

        printf("\nMessage chiffre : %s\n", texte);
}

void Dechiffrage_Vigenere(char texte [255], char Clee [255]) {

    char nouvelleClee[255];

    for(int i = 0, j = 0; i < strlen(texte); i++, j++){
      if (j == strlen(Clee))
          j = 0;

      if (texte[i] == ' ') {
          nouvelleClee[i] = ' ';
          j = j - 1;

      } else { 
        nouvelleClee[i] = Clee[j];
      }
}

    for(int i = 0; i < strlen(texte); i++) {
        int i3;
        int i2;
        int k;

         for (i2 = 0; i2 == 27 || AlphabetMinus[i2] != texte[i]; i2++) {
            k = 0;
         }

         for (i3 = 0; AlphabetMinus[i3] != nouvelleClee[i] || i3 == 27; i3++) {
         }

         if (i2 > 27) {
          for (i2 = 0; AlphabetMajus[i2] != texte[i] || i2 == 27; i2++) {
            k = 1;
          }
        }

        if (i3 > 27) { 
         for (i3 = 0; AlphabetMajus[i3] != nouvelleClee[i] || i3 == 27; i3++) {
         }
        }

        if (texte[i] == 'a') {
          k = 0;
        }
        
        if (texte[i] == 'A') {
          k = 1;
        }

         if (i2 < 27 && i3 < 27) {
            if (texte[i] == ' ') {
               texte[i] = ' '; 
            } else if (k == 0) {
                if (i2 >= i3) {
                  texte[i] = AlphabetMinus[i2 - i3];
                } else {
                  texte[i] = AlphabetMinus[(i2 - i3) + 26];
                }
            } else {
                if (i2 >= i3) {
                  texte[i] = AlphabetMajus[i2 - i3];
                } else {
                  texte[i] = AlphabetMajus[(i2 - i3) + 26];
                }
            }
         }
      }

      printf("Déchiffrement du message codé : %s\n", texte);
}